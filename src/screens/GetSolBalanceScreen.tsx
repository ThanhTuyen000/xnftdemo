import { Text, FlatList } from "react-native";
import { useEffect, useState } from "react";
import tw from "twrnc";

import { Screen } from "../components/Screen";
import { TOKEN_PROGRAM_ID } from "@solana/spl-token";
import { Connection, PublicKey, Transaction, LAMPORTS_PER_SOL, SystemProgram, sendAndConfirmTransaction } from "@solana/web3.js";
import * as SPLToken from "@solana/spl-token";
import { useSolanaConnection, usePublicKeys, } from "react-xnft";

import { Form, FormData } from "../components/Form";

const connection = new Connection("https://api.devnet.solana.com");


export function SolBalanceScreen() {
  const [pk, setPk] = useState<PublicKey>();
  const pks = usePublicKeys() as unknown as { solana: string };
  const [balance, setBalance] = useState<number>(0);
  const [signature, setSignature] = useState("");
  const [loading, setLoading] = useState(false);
  useEffect(() => {
    const pkt = pks ? new PublicKey(pks?.solana) : undefined;
    setPk(pkt);
  }, [pks]);

  useEffect(() => {
    async function getBalance() {
      if (!pk) {
        console.error('Không thể kết nối tới ví.');
        return;
      }

      try {
        // Lấy thông tin số dư SOL của ví của bạn
        const yourBalance = await connection.getBalance(pk);
        setBalance(yourBalance / LAMPORTS_PER_SOL);
      } catch (error) {
        console.error('Lỗi khi lấy số dư:', error);
      }
    }

    getBalance();
  }, [pk]);

  async function handleSubmit(formData: FormData) {
    if (!pk) {
      console.error('Không thể kết nối tới ví.');
      return;
    };

    const destinationAddress = formData.destinationAddress;
    const amount = formData.amount;

    if (balance < amount) {
      console.error('Không đủ số dư.');
      return;
    }

    // Tạo transaction để gửi SOL
    const transaction = new Transaction().add(
      SystemProgram.transfer({
        fromPubkey: pk,
        toPubkey: new PublicKey(destinationAddress),
        lamports: amount * LAMPORTS_PER_SOL,
      })
    );

    setLoading(true);

    const sx = await window.xnft.solana.send(transaction);

    // if (!signature) {
    //   setLoading(false);
    //   return;
    // }

    const newBalance = await connection.getBalance(pk);
    setBalance(newBalance / LAMPORTS_PER_SOL);

    setSignature(sx);

    setLoading(false);
  }

  return (
    <Screen>

      <h1>Số dư: {balance} SOL</h1>
      <Form onSubmit={handleSubmit} />
      <Text style={tw`mb-4`}>
        Signature: {signature}
      </Text>

    </Screen>
  );
}
