import { Button } from "react-native";

import { Screen } from "../components/Screen";

import { Metaplex, walletAdapterIdentity } from "@metaplex-foundation/js";
import { Connection, clusterApiUrl } from "@solana/web3.js";
import { useSolanaConnection } from "../hooks/xnft-hooks";
import { useMemo, useState } from "react";

export function MintNFTScreen() {
    const [isLoading, setIsLoading] = useState(false);
    const connection = useSolanaConnection();
    const metaplex = useMemo(() => {
        if (connection) {
            const _metaplex = new Metaplex(connection!).use(walletAdapterIdentity(window.xnft.solana));
            return _metaplex;
        }
        return undefined;
    }, [connection])
    return (
        <Screen>
            <Button title="Mint NFT" onPress={async () => {
                if (metaplex !== undefined) {
                    setIsLoading(true);
                    const nft = await metaplex.nfts().create({
                        uri: `https://nft.megatunger.com/api/json?image=${encodeURIComponent("https://yocto.scrolller.com/archer-by-catbuck-j-3mxjvvky2v-951x1500.jpg")}`,
                        name: "Test 123",
                        sellerFeeBasisPoints: 500, // Represents 5.00%.
                    });
                    console.log(nft);
                    setIsLoading(false);
                }
            }} />
        </Screen>
    );
}
