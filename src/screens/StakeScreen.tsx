import { Button, Text } from "react-native";
import tw from "twrnc";
import { Screen } from "../components/Screen";
import {
  AnchorProvider,
  BN,
  ProgramAccount,
  Provider,
  web3,
} from '@coral-xyz/anchor'

import NodeWallet from '@coral-xyz/anchor/dist/cjs/nodewallet'

import {
  Connection,
  Keypair,
  LAMPORTS_PER_SOL,
  PublicKey,
  StakeProgram,
  Transaction,
  TransactionMessage,
  VersionedTransaction
} from '@solana/web3.js'

import { Marinade, MarinadeConfig, MarinadeUtils } from '@marinade.finance/marinade-ts-sdk'
import { useSolanaConnection, usePublicKeys, } from "react-xnft";
import { useEffect, useState } from "react";

const connection = new Connection("https://api.devnet.solana.com");

import { NativeStakingConfig, NativeStakingSDK } from '@marinade.finance/native-staking-sdk'
const config = new NativeStakingConfig({ connection: connection })
const sdk = new NativeStakingSDK(config)

import { useWallet } from '@solana/wallet-adapter-react'

export function StakeScreen() {

  const [pk, setPk] = useState<PublicKey>();
  const pks = usePublicKeys() as unknown as { solana: string };
  const [marinade, setMarinade] = useState<Marinade | null>(null)
  const [processingTransaction, setProcessingTransaction] = useState(false)
  const [signature, setSignature] = useState("");
  const [signature1, setSignature1] = useState("");
  const [signature2, setSignature2] = useState("");
  const [signature3, setSignature3] = useState("");
  const [signature4, setSignature4] = useState("");

  useEffect(() => {
    const pkt = pks ? new PublicKey(pks?.solana) : undefined;
    setPk(pkt);
  }, [pks]);

  useEffect(() => {
    if (!pk) {
      setMarinade(null)
      return
    }

    // !!! You have to put in your own Referral Code on the line below !!!
    // !!! Then uncomment the `referralCode` configuration option !!!
    const config = new MarinadeConfig({ connection, publicKey: pk/*, referralCode*/ })
    const marinade = new Marinade(config)

    setMarinade(marinade)
  }, [connection, pk])

  return (
    <Screen>

      <Button title="Deposit" onPress={async () => {
        console.log("Deposit")
        console.log(pk)
        console.log(marinade)
        if (marinade) {
          const { transaction } = await marinade.deposit(MarinadeUtils.solToLamports(0.01))
          const sx = await window.xnft.solana.send(transaction);
          setSignature(sx);
        }
      }} />

      <Text style={tw`mb-4`}>
        Signature: {signature}
      </Text>

      <Button title="DepositStakeAccount" onPress={async () => {
        console.log("DepositStakeAccount")
        console.log(pk)
        console.log(marinade)
        if (marinade) {
          const { transaction } = await marinade.depositStakeAccount(new web3.PublicKey("EGh5EJBsz6dJAPTvAdS53B2DXv29xWPiVGmT4C5WLJ22"))
          const sx = await window.xnft.solana.send(transaction);
          setSignature1(sx);
        }
      }} />

      <Text style={tw`mb-4`}>
        Signature1: {signature1}
      </Text>

      <Button title="Unstake" onPress={async () => {
        console.log("Unstake")
        console.log(pk)
        console.log(marinade)
        if (marinade) {
          const { transaction } = await marinade.liquidUnstake(MarinadeUtils.solToLamports(0.01))
          const sx = await window.xnft.solana.send(transaction);
          setSignature2(sx);
        }
      }} />

      <Text style={tw`mb-4`}>
        Signature2: {signature2}
      </Text>

      <Button title="Stake Native" onPress={async () => {
        console.log("Stake Native")

        if (pk) {
          const { blockhash } = await connection.getLatestBlockhash()
          const { createAuthorizedStake, stakeKeypair } = sdk.buildCreateAuthorizedStakeInstructions(pk, MarinadeUtils.solToLamports(0.02))
          const versionedMessage = 1;
          const tx = new VersionedTransaction(new TransactionMessage({
            payerKey: pk,
            recentBlockhash: blockhash,
            instructions: createAuthorizedStake,
          }).compileToV0Message())
          tx.sign([stakeKeypair])
          const sx = await window.xnft.solana.send(tx);
          setSignature3(sx);
        }
      }} />
      <Text style={tw`mb-4`}>
        Signature3: {signature3}
      </Text>

      <Button title="Get Native" onPress={async () => {
        console.log("Get Native")
        if (pk) {
          console.log(await sdk.getStakeAccounts(pk));
          console.log(await sdk.fetchRewards(pk));
        }
      }} />
      <Text style={tw`mb-4`}>
        ---------------------------
      </Text>

      <Button title="Unstake Native" onPress={async () => {
        console.log("Unstake Native")
        if (pk) {
          const { payFees, onPaid } = await sdk.initPrepareForRevoke(pk, MarinadeUtils.solToLamports(0.01)) // pass `null` instead of `amount` to prepare everything for unstake
          const { blockhash } = await connection.getLatestBlockhash()
          const tx = new VersionedTransaction(new TransactionMessage({
            payerKey: pk,
            recentBlockhash: blockhash,
            instructions: payFees,
          }).compileToV0Message())
          const sx = await window.xnft.solana.send(tx);
          console.log(sx)
          await connection.confirmTransaction(sx, 'finalized') // wait for the payment to go through
          await onPaid(sx) // notify our BE that the user has paid, so we can prepare some/all stake accounts (based on the input amount) to be merged them into a single stake account (at the beginning of the next epoch), so the SOL can be easily withdrawn (or stake account claimed if it is Locked).
          setSignature4(sx);
        }
      }} />
      <Text style={tw`mb-4`}>
        Signature4: {signature4}
      </Text>

    </Screen>
  );
}
