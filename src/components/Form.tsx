import React from 'react'

interface FormProps {
    onSubmit: (data: FormData) => void;
}

export interface FormData {
    destinationAddress: string;
    amount: number;
}

export function Form({ onSubmit }: FormProps) {
    const [formData, setFormData] = React.useState<FormData>({ destinationAddress: '', amount: 0 });
    const [errors, setErrors] = React.useState<{ [key: string]: string }>({});

    function handleInputChange(event: React.ChangeEvent<HTMLInputElement>) {
        const { name, value } = event.target;
        setFormData({ ...formData, [name]: value });
        // Clear the error message when the input changes
        setErrors({ ...errors, [name]: '' });
    }

    function handleSubmit(event: React.FormEvent<HTMLFormElement>) {
        event.preventDefault();
        // Validate the form data before submitting
        const validationErrors = validateForm(formData);
        if (Object.keys(validationErrors).length === 0) {
            onSubmit(formData);
        } else {
            // Set the errors state if there are validation errors
            setErrors(validationErrors);
        }
    }

    function validateForm(data: FormData) {
        const errors: { [key: string]: string } = {};

        if (!data.destinationAddress) {
            errors.destinationAddress = 'Địa chỉ đến không được để trống.';
        }

        if (data.amount <= 0) {
            errors.amount = 'Số lượng phải lớn hơn 0.';
        }

        return errors;
    }

    return (
        <form onSubmit={handleSubmit}>
            <label>
                Địa chỉ đến:
                <input type="text" name="destinationAddress" value={formData.destinationAddress} onChange={handleInputChange} />
                {errors.destinationAddress && <span style={{ color: 'red' }}>{errors.destinationAddress}</span>}
            </label>
            <br />
            <label>
                Số lượng:
                <input type="number" name="amount" value={formData.amount} onChange={handleInputChange} />
                {errors.amount && <span style={{ color: 'red' }}>{errors.amount}</span>}
            </label>
            <br />
            <button type="submit">Gửi</button>
        </form>
    );
}